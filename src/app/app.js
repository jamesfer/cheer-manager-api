'use strict';

var router = require('express').Router();


/**
 * Logging middleware
 */
 // TODO change this to use middleware function
 // EG router.use(function(req, res, next))???
 // Not sure if router actually has this function
router.all(/.*/, function(req, res, next) {
	console.log(req.method + ' - ' + req.originalUrl);
	next();
});


// Enable cors
router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", process.env.CORS_ALLOW_ORIGIN || "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});


// Mount each of the api paths
router.use('/members', require('./members/members.router.js'));
router.use('/teams', require('./teams/teams.router.js'));
router.use('/competitions', require('./competitions/competitions.router.js'));


// Custom error handler for api requests
router.use(function(err, req, res, next) {
	console.log('An error occurred during processing.');
	console.log(err);
	if (err.fileName) {
		console.log(err.fileName + " @" + err.lineNumber);
	}



	if (err.status) {
		res.status(err.status);
	}
	else {
		res.status(500);
	}
	res.json(err);
});


module.exports = router;
