'use strict';

var _ = require('lodash');
var Q = require('q');
var db = require('../../lib/seraph.js');
var Controller = require('../../lib/controller.js');
var competitionsModel = require('./competitions.model.js');
var teamsController = require('../teams/teams.controller.js');


class CompetitionsController extends Controller {
  constructor() {
    super(competitionsModel);
  }

  getTeams(id) {
    return this.fetchRelated(id, 'in', 'COMPETING_AT', teamsController);
  }

  updateTeams(compId, otherTeams) {
    return this.updateRelations(compId, 'in', 'COMPETING_AT', otherTeams);
  }

  getMembers(id) {
    // Fetch all teams of this comp
    return this.getTeams(id)
      .then(function(teams) {
        var ops = [];

        // For each team, fetch its members
        _.each(teams, function(team) {
          // Add the promise to the array of operations
          ops.push(
            teamsController.getMembers(team.id)
              .then(function(members) {
                // Add the members to the team object
                team.members = members;
              })
          );
        });

        // Pass the teams list onto the next handler
        return Q.all(ops)
          .then(function() {
            return teams;
          });
      })
      .then(function(teams) {
        var membersAggregate = {};

        // Loop over each team
        _.each(teams, function(team) {
          // Loop over each member in the team
          _.each(team.members, function(member) {
            // The member was not present in the aggregate
            if (membersAggregate[member.id] === undefined) {
              member.teams = [];
              membersAggregate[member.id] = member;
            }

            var plainTeam = _.omit(team, 'members');

            // Add the team to the member's list
            membersAggregate[member.id].teams.push(plainTeam);
          });
        });

        // Return just the array of values of the aggregate
        return _.values(membersAggregate);
      });
  }
}

module.exports = new CompetitionsController();
