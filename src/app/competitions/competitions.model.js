'use strict';

var _ = require('lodash');
var model = require('seraph-model');
var db = require('../../lib/seraph.js');


var Competitions = model(db, 'CM_COMPETITION');
Competitions.fields = ['name', 'entryFee', 'crossoverFee'];


module.exports = Competitions;
