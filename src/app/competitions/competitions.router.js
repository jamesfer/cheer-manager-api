'use strict';

var _ = require('lodash');
var Q = require('q');
var competitionsController = require('./competitions.controller.js');
var promiseResponse = require('../../lib/promiseResponse');
var router = require('express').Router();
module.exports = router;



router.get('/',
	promiseResponse(function(req, res) {
		return competitionsController.query(req.query.fields);
	})
);


// TODO return an error if the name is already in use
router.post('/',
	promiseResponse(function(req, res) {
		return competitionsController.save(req.body);
	})
);


// TODO return an error if the team does not exist
router.get('/:id',
	promiseResponse(function(req, res) {
		return competitionsController.get(req.params.id);
	})
);


// TODO return an error if the team does not exist
// TODO return an error if the name is already in use
router.post('/:id',
	promiseResponse(function(req, res) {
		var data = _.merge(req.body, {id: req.params.id});
		return competitionsController.save(data);
	})
);


// TODO Return an error if the team does not exists
router.delete('/:id',
	promiseResponse(function(req, res) {
		return competitionsController.delete(req.params.id);
	})
);


router.get('/:id/teams',
	promiseResponse(function(req, res) {
		return competitionsController.getTeams(req.params.id);
	})
);


router.put('/:id/teams',
	promiseResponse(function(req, res) {
		return competitionsController.updateTeams(req.params.id, req.body.teams);
	})
);


router.get('/:id/members',
	promiseResponse(function(req, res) {
		return competitionsController.getMembers(req.params.id);
	})
);
