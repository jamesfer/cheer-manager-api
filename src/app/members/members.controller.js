'use strict';

var _ = require('lodash');
var Q = require('q');
var Controller = require('../../lib/controller.js');
var membersModel = require('./members.model.js');


class MembersController extends Controller {
  constructor() {
    super(membersModel);
  }


  bulkAdd(csvString) {
    var lines = csvString.split(/\r\n|\r|\n/g);
    var members = [];

    _.each(lines, function(line) {
      var names = line.split(',');
      if (names.length === 2) {
        members.push({
          firstName: _.trim(names[0]),
          lastName: _.trim(names[1])
        });
      }
    });

    // Now save all of the members
    var ops = [];
    _.each(members, function(member) {
      ops.push(
        Q.ninvoke(membersModel, 'save', member)
      );
    });

    return Q.all(ops)
      .then(function() {
        return true;
      });
  }

  /**
   * Returns all matching team nodes
   *
   * @param   {Object} options - Configuration options.
   * @param   {Array.<string>} options.fields - The fields of each node to query for.
   *
   * @returns {Promise.<Array>} - Array of node information that is returned from the database.
   */
  // query(options) {
  //   options = _.merge({
  //     fields : []
  //   }, options);
  //
  //   return Q.ninvoke(membersModel, 'findAll');
  // }


  /**
   * Returns a single matching node.
   *
   * @param   {Integer} id - id of the node to fetch
   *
   * @returns {Promise.<Object>} - Node information returned from the database.
   */
  // get(id) {
  //   id = parseInt(id);
  //
  //   // TODO return error if node does not exist
  //   return Q.ninvoke(membersModel, 'read', id);
  // }
  //
  //
  // save(data) {
  //   if (data.id !== undefined) {
  //     data.id = parseInt(data.id);
  //   }
  //
  //   return Q.ninvoke(membersModel, 'save', data);
  // }
  //
  //
  // delete(id) {
  //   id = parseInt(id);
  //
  //   return Q.ninvoke(db, 'delete', id, true);
  // }
}

module.exports = new MembersController();
