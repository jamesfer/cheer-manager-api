'use strict';

var _ = require('lodash');
var model = require('seraph-model');
var db = require('../../lib/seraph.js');


var Members = model(db, 'CM_MEMBER');
Members.fields  = ['firstName', 'lastName'];


module.exports = Members;
