'use strict';

// var restModel = require('../lib/restModel');
// var parambulator = require('parambulator');
// var extract = require('extract');
// var clean = require('obj-clean');

// var model = restModel.createModel({label: 'MEMBER', verbose: true});

// var members = require('../models/members');
// var teams = require('../models/teams');
// var payments = require('../models/payments');
// var trainings = require('../models/trainings');
// var promiseDB = require('../lib/promiseDB');
// var utils = require('../lib/routeUtils');

var _ = require('lodash');
var Q = require('q');
var membersController = require('./members.controller.js');
var promiseResponse = require('../../lib/promiseResponse');
var router = require('express').Router();
module.exports = router;




router.get('/',
	promiseResponse(function(req, res) {
		return membersController.query(req.query.fields);
	})
);


// TODO return an error if the name is already in use
router.post('/',
	promiseResponse(function(req, res) {
		return membersController.save(req.body);
	})
);


router.post('/bulkAdd',
	promiseResponse(function(req, res) {
		return membersController.bulkAdd(req.body.members)
	})
);


// TODO return an error if the team does not exist
router.get('/:id',
	promiseResponse(function(req, res) {
		return membersController.get(req.params.id);
	})
);


// TODO return an error if the team does not exist
// TODO return an error if the name is already in use
router.post('/:id',
	promiseResponse(function(req, res) {
		var data = _.merge(req.body, {id: req.params.id});
		return membersController.save(data);
	})
);


// TODO Return an error if the team does not exists
router.delete('/:id',
	promiseResponse(function(req, res) {
		return membersController.delete(req.params.id);
	})
);























//
// router.get('/',
// 	promiseResponse(function(req, res) {
// 		var query = '';
// 		var params = {};
//
// 		query = 'MATCH (m:MEMBER';
//
// 		if (!req.query.all) {
// 			// Return only accepted members
// 			query += ' {status: \'accepted\'}';
// 		}
//
// 		query += ')';
//
// 		if (req.query.onTeam !== undefined) {
// 			params.teamId = parseInt(req.query.onTeam);
//
// 			if (!req.query.includeNotOnTeam) {
// 				// Only return members that are part of a certain team
// 				query += '-[:MEMBER_OF]->(t:TEAM {id: {teamId}})';
// 			}
// 		}
//
// 		query += ' RETURN m.id, m.firstName, m.lastName, m.status';
//
// 		if (req.query.onTeam !== undefined && req.query.includeNotOnTeam) {
// 			// Add a variable to indicate if the member is part of the team
// 			query += ', exists((m)-[:MEMBER_OF]->(:TEAM {id: {teamId}})) AS onTeam';
// 		}
//
// 		return promiseDB.queryFormatted(query, params);
// 	})
// );
//
//
// router.post('/', function(req, res) {
// 	console.log('POST to /api/members/');
// 	res.send({});
// });
// router.get('/:id', function(req, res) {
// 	console.log('GET to /members/:id');
// 	res.send({});
// });
//
//
//
// // Special route that searches for a user based on their Facebook id
// // Then updates their details or creates a new one if not found
// // It can only be used to update first and last names
// // Upon a successful update, will return all the details of that user
// // router.post('/updateFromFacebook', function(req, res) {
// // 	// Extract properties
// // 	var props = extract(clean(req.body) , ['facebookId', 'firstName', 'lastName']);
// //
// // 	// Confirm params are valid
// // 	var paramcheck = parambulator({
// // 		facebookId: {
// // 			required$: true,
// // 			notempty$: true
// // 		},
// // 		firstName: {
// // 			type$: 'string',
// // 		},
// // 		lastName: {
// // 			type$: 'string',
// // 		}
// // 	});
// //
// // 	paramcheck.validate(props, function(err) {
// // 		if (err) {
// // 			console.log('Validation Error');
// // 			res.status(400).send(err);
// // 		}
// //
// // 		// Check passed
// // 		else {
// // 			model.queryFormatted({
// // 				query: 'MATCH (m:MEMBER) WITH coalesce(max(m.id) + 1, 0) AS newId MERGE (m:MEMBER {facebookId: {facebookId}}) ON CREATE SET m.id = newId, m.status = "requested" SET m += {props} WITH m OPTIONAL MATCH (m)-[:CAN]->(p:PERMISSION) RETURN m.id, m.facebookId, m.firstName, m.lastName, collect(p.name) AS permissions',
// // 				params: {
// // 					facebookId: props.facebookId,
// // 					props: extract(props, ['firstName', 'lastName'])
// // 				},
// // 				err: function(err) {
// // 					res.status(500).send('Database Error');
// // 				},
// // 				success: function(results) {
// // 					console.log('Updated member record!');
// // 					console.log(results);
// // 					res.json(extract(results, ['id', 'facebookId', 'firstName', 'lastName', 'permissions']));
// // 				}
// // 			});
// // 		}
// // 	});
// // });
//
//
// router.post('/:id', function(req, res) {
// 	// Extract only accepted properties
// 	var props = extract(req.body, ['status']);
//
// 	if (props.status) {
// 		props.status = props.status.toLowerCase();
// 	}
//
//
// 	// Confirm properties are valid
// 	var paramcheck = parambulator({
// 		status: {
// 			enum$: ['accepted', 'requested', 'denied']
// 		}
// 	});
//
// 	paramcheck.validate(props, function(err) {
// 		if (err) {
// 			console.log('Validation Error');
// 			res.status(400).send(err);
// 		}
//
// 		// Check passed
// 		else {
// 			model.queryFormatted({
// 				query: 'MATCH (m:MEMBER {id: {id}}) SET m += {props}',
// 				params: {id: req.params.id, props: props},
// 				err: function(err) {
// 					res.status(500).json('Database error');
// 				},
// 				success: function(results) {
// 					res.json({id: req.params.id});
// 				}
// 			});
// 		}
// 	});
// });
//
//
// router.delete('/:id', function(req, res) {
// 	console.log('DELETE to /api/members/:id');
// 	res.send({});
// });
//
//
// router.get('/:id/teams',
// 	utils.requireAuth,
// 	utils.genPermissionsCheck(['ViewAllMembers']),
// 	utils.genValidationCheck(['props'], {
// 		props: {
// 			type$: 'array',
// 			'*': {
// 				type$: 'string'
// 			}
// 		}
// 	}),
// 	utils.convertToNumber('id'),
// 	promiseResponse(function(req, res) {
// 		return teams.getForMember(req.params.id, req.query.props);
// 	})
// );
//
//
// router.get('/:id/payments',
// 	utils.requireAuth,
// 	utils.genPermissionsCheck(['ViewAllMembers']),
// 	utils.genValidationCheck(['props'], {
// 		props: {
// 			type$: 'array',
// 			'*': {
// 				type$: 'string'
// 			}
// 		}
// 	}),
// 	utils.convertToNumber('id'),
// 	promiseResponse(function(req, res) {
// 		return payments.getForMember(req.params.id, req.query.props);
// 	})
// );
//
//
// router.get('/:id/trainings',
// 	utils.requireAuth,
// 	utils.genPermissionsCheck(['ViewAllMembers']),
// 	utils.genValidationCheck(['props'], {
// 		props: {
// 			type$: 'array',
// 			'*': {
// 				type$: 'string'
// 			}
// 		}
// 	}),
// 	utils.convertToNumber('id'),
// 	promiseResponse(function(req, res) {
// 		return trainings.getForMember(req.params.id, req.query.props);
// 	})
// );
