'use strict';

var _ = require('lodash');
var Q = require('q');
var Controller = require('../../lib/controller.js');
var teamsModel = require('./teams.model.js');
var membersController = require('../members/members.controller.js');


class TeamsController extends Controller {
  constructor() {
    super(teamsModel);
  }


  /**
   * Returns all matching team nodes
   *
   * @param   {Object} options - Configuration options.
   * @param   {Array.<string>} options.fields - The fields of each node to query for.
   *
   * @returns {Promise.<Array>} - Array of node information that is returned from the database.
   */
  // query(options) {
  //   options = _.merge({
  //     fields : []
  //   }, options);
  //
  //   return Q.ninvoke(teamsModel, 'findAll', {
  //     otherVars: options.fields
  //   });
  // }


  /**
   * Returns a single matching node.
   *
   * @param   {Integer} id - id of the node to fetch
   *
   * @returns {Promise.<Object>} - Node information returned from the database.
   */
  // get(id) {
  //   id = parseInt(id);
  //
  //   // TODO return error if node does not exist
  //   return Q.ninvoke(teamsModel, 'read', id);
  // }


  // save(data) {
  //   if (data.id !== undefined) {
  //     data.id = parseInt(data.id);
  //   }
  //
  //   return Q.ninvoke(teamsModel, 'save', data);
  // }
  //
  //
  // delete(id) {
  //   id = parseInt(id);
  //
  //   return Q.ninvoke(db, 'delete', id, true);
  // }


  getMembers(id) {
    return this.fetchRelated(id, 'in', 'MEMBER_OF', membersController);
    // id = parseInt(id);
    //
    // return Q.ninvoke(db, 'relationships', id, 'in', 'MEMBER_OF')
    //   .then(function(relations) {
    //     var members = [];
    //     var ops = [];
    //
    //     _.each(relations, function(rel) {
    //       ops.push(
    //         membersController.get(rel.start)
    //           .then(function(node) {
    //             members.push(node);
    //           })
    //       );
    //     });
    //
    //     return Q.all(ops).then(function() {
    //       return members;
    //     });
    //   });
  }


  updateMembers(teamId, otherMembers) {
    return this.updateRelations(teamId, 'in', 'MEMBER_OF', otherMembers);
    // teamId = parseInt(teamId);
    //
    // return Q.ninvoke(db, 'relationships', teamId, 'in', 'MEMBER_OF')
    //   .then(function(relationships) {
    //     var operations = [];
    //
    //     // For these relationships, start is the member, end is the team
    //
    //     // Filter should return all relationships that are not a part of otherMembers
    //     var relsToDelete = _.filter(relationships, function(rel) {
    //       return otherMembers.indexOf(rel.start) === -1;
    //     });
    //
    //     // Filter should return all members that are not a part of relationships
    //     var membersToAdd = _.filter(otherMembers, function(id) {
    //       // Returns true if no relationship from starting at that node can be found
    //       return _.filter(relationships, ['start', id]).length === 0;
    //     });
    //
    //     _.each(relsToDelete, function(rel) {
    //       operations.push(Q.ninvoke(db.rel, 'delete', rel.id));
    //     });
    //
    //     _.each(membersToAdd, function(memId) {
    //       operations.push(Q.ninvoke(db.rel, 'create', memId, 'MEMBER_OF', teamId));
    //     });
    //
    //     return Q.all(operations);
    //   })
    //   .then(function() {
    //     return true;
    //   });
  }
}

module.exports = new TeamsController();
