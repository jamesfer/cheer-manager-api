'use strict';

var _ = require('lodash');
var model = require('seraph-model');
var db = require('../../lib/seraph.js');


var Teams = model(db, 'CM_TEAM');
Teams.fields  = ['name'];


module.exports = Teams;



// class Teams extends Model {
//   constructor() {
//     super({
//       label: 'TEAM',
//       defaultProps: ['id', 'name']
//     });
//   }
//
//   getForPayment(paymentId, props) {
//     if (!_.isArray(props)) {
//       props = [];
//     }
//
//     var options = {
//       otherNodeId: paymentId,
//       otherNodeLabel: 'PAYMENT',
//       relationshipLabel: 'CAN_PAY',
//       direction: 'outgoing'
//     };
//
//     return this.fetchRelatedTo(options, ['required'], props);
//   }
//
//   getForMember(memberId, props) {
//     var options = {
//       otherNodeId: memberId,
//       otherNodeLabel: 'MEMBER',
//       relationshipLabel: 'MEMBER_OF',
//       direction: 'incoming'
//     };
//
//     return this.fetchRelatedTo(options, [], props);
//   }
//
//   getForTraining(trainingId, props) {
//     var options = {
//       otherNodeId: trainingId,
//       otherNodeLabel: 'TRAINING',
//       relationshipLabel: 'CAN_ATTEND',
//       direction: 'outgoing'
//     };
//
//     return this.fetchRelatedTo(options, ['required'], props);
//   }
// }

// module.exports = new Teams();
