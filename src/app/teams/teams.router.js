'use strict';



// var promiseDB = require('../lib/promiseDB');
// var utils = require('../lib/routeUtils');
// var permissionsUtils = require('..lib/permissionUtils.js');
// var errors = require('../lib/errors.js');

var _ = require('lodash');
var Q = require('q');
var teamsController = require('./teams.controller.js');
var promiseResponse = require('../../lib/promiseResponse');
var router = require('express').Router();
module.exports = router;



router.get('/',
	promiseResponse(function(req, res) {
		return teamsController.query(req.query.fields);
	})
);


// TODO return an error if the name is already in use
router.post('/',
	promiseResponse(function(req, res) {
		return teamsController.save(req.body);
	})
);


// TODO return an error if the team does not exist
router.get('/:id',
	promiseResponse(function(req, res) {
		return teamsController.get(req.params.id);
	})
);


// TODO return an error if the team does not exist
// TODO return an error if the name is already in use
router.post('/:id',
	promiseResponse(function(req, res) {
		var data = _.merge(req.body, {id: req.params.id});
		return teamsController.save(data);
	})
);


// TODO Return an error if the team does not exists
router.delete('/:id',
	promiseResponse(function(req, res) {
		return teamsController.delete(req.params.id);
	})
);


router.get('/:id/members',
	promiseResponse(function(req, res) {
		return teamsController.getMembers(req.params.id);
	})
);


router.put('/:id/members',
	promiseResponse(function(req, res) {
		return teamsController.updateMembers(req.params.id, req.body.members);
	})
);

//
// router.get('/:id/members', function(req, res) {
// 	// TODO maybe require full to be a boolean
// 	var full = req.query.full;
//
// 	// When full is specified, all members are returned with a boolean specifiying whether they are on the team
// 	if (full) {
// 		model.queryFormatted({
// 			query: 'MATCH (m:MEMBER) WHERE m.status = "accepted" RETURN m.id, m.firstName, m.lastName, exists((m)-[:MEMBER_OF]->(:TEAM {id: {id}})) AS onTeam',
// 			params: {id: req.params.id},
// 			returnArray: true,
// 			error: function(err) {
// 				res.status(500).send('Database Error');
// 			},
// 			success: function(results) {
// 				res.json(results);
// 			}
// 		});
// 	}
// 	// Otherwise return only members that are on a team
// 	else {
// 		model.queryFormatted({
// 			query: 'MATCH (m:MEMBER)-[:MEMBER_OF]->(t:TEAM {id: {id}}) WHERE m.status = "accepted" RETURN m.id, m.firstName, m.lastName',
// 			params: {id: req.params.id},
// 			returnArray: true,
// 			error: function(err) {
// 				res.status(500).send('Database Error');
// 			},
// 			success: function(results) {
// 				res.json(results);
// 			}
// 		});
// 	}
// });


// router.put('/:id/members',
// 	utils.requireAuth,
// 	utils.genPermissionsCheck(['EditAllTeams']),
// 	utils.genValidationCheck(['members'], {
// 		members: {
// 			required$: true,
// 			type$: 'array',
// 			'*': {
// 				type$: 'integer'
// 			},
// 			uniq$: true
// 		}
// 	}),
// 	promiseResponse(function(req, res) {
// 		console.log(req.body.members);
// 		// Update the members
// 		// First delete all members not in the list
// 		// Delete all unneeded relationships
// 		return promiseDB.queryFormatted([
// 				'MATCH (m:MEMBER)-[r:MEMBER_OF]->(t:TEAM {id: 0})',
// 				'WHERE NOT m.id IN {membersList}',
// 				'DELETE r'
// 			],
// 			{id: req.params.id, membersList: req.body.members}
// 		).then(function() {
// 			return promiseDB.queryFormatted([
// 					// Find all members that need a new relationship
// 					'MATCH (t:TEAM {id: 0})',
// 					'WITH t',
// 					'MATCH (m:MEMBER)',
// 					'WHERE m.id IN {membersList} AND NOT exists((m)-[:MEMBER_OF]->(t))',
// 					'CREATE (m)-[:MEMBER_OF]->(t)'
// 				],
// 				{id: req.params.id, membersList: req.body.members}
// 			);
// 		}).then(function() {
// 			return {};
// 		});
// 	})
// );



// Find the first available id
// 'MATCH (:MEMBER)-[r:MEMBER_OF]->(:TEAM)',
// 'WITH coalesce(max(r.id) + 1, 0) AS nextId, t',
// // Find all members that need a new relationship
// 'MATCH (m:MEMBER)',
// 'WHERE m.id IN {membersList} AND NOT exists((m)-[:MEMBER_OF]->(t))',
// 'WITH collect(m) AS newMembers, nextId, t',
// // Loop from 0 to size(newMembers) - 1
// 'UNWIND range(0, size(newMembers) - 1) AS counter',
// // newMember = newMembers[i] and relId = nextId + i;
// 'WITH newMembers[counter] AS newMember, nextId + counter AS relId, t',
// 'CREATE (newMember)-[:MEMBER_OF {id: relId}]->(t)'
