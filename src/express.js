"use strict";

require('dotenv').config();
var express = require('express');
var bodyParser = require('body-parser');

var app = express();


// Body parser
app.use(bodyParser.json());

// The api paths
app.use('/api', require('./app/app.js'));


// Server errors
app.use(function(err, req, res, next) {
	console.error(err.stack);
	res.status(500).send('<h1>Server error</h1>');
});


// Start the server
var server = app.listen(3000, 'localhost', function() {
	var host = server.address().address;
  var port = server.address().port;

	console.log('App is running at http://%s:%s', host, port);
});
