'use strict';

var _ = require('lodash');
var Q = require('q');
var db = require('./seraph.js');


class Controller {
  constructor(model) {
    this.model = model;
  }


  /**
   * Returns all matching team nodes
   *
   * @param   {Object} options - Configuration options.
   * @param   {Array.<string>} options.fields - The fields of each node to query for.
   *
   * @returns {Promise.<Array>} - Array of node information that is returned from the database.
   */
  query(options) {
    options = _.merge({
      fields : []
    }, options);

    return Q.ninvoke(this.model, 'findAll', {
      otherVars: options.fields
    });
  }


  /**
   * Returns a single matching node.
   *
   * @param   {Integer} id - id of the node to fetch
   *
   * @returns {Promise.<Object>} - Node information returned from the database.
   */
  get(id) {
    id = parseInt(id);

    // TODO return error if node does not exist
    return Q.ninvoke(this.model, 'read', id);
  }


  save(data) {
    if (data.id !== undefined) {
      data.id = parseInt(data.id);
    }

    return Q.ninvoke(this.model, 'save', data);
  }


  delete(id) {
    id = parseInt(id);

    return Q.ninvoke(db, 'delete', id, true);
  }


  updateRelations(thisId, direction, label, otherIds) {
    thisId = parseInt(thisId);

    return Q.ninvoke(db, 'relationships', thisId, direction, label)
      .then(function(relationships) {
        var operations = [];

        // If the relation is incoming to this model,
        // Then the id property will be start.
        // And vice versa
        var otherIdProp = (direction === 'in') ? 'start' : 'end';

        // Find all relations that don't start at a node in otherIds
        var relsToDelete = _.filter(relationships, function(rel) {
          return otherIds.indexOf(rel[otherIdProp]) === -1;
        });

        // Find all ids that don't alread have a relation
        var idsToRelate = _.filter(otherIds, function(id) {
          // Returns true if no relationship from starting at that node can be found
          return _.filter(relationships, [otherIdProp, id]).length === 0;
        });

        // Delete rels
        _.each(relsToDelete, function(rel) {
          operations.push(Q.ninvoke(db.rel, 'delete', rel.id));
        });

        // Create rels
        _.each(idsToRelate, function(memId) {
          operations.push(Q.ninvoke(db.rel, 'create', memId, label, thisId));
        });

        // Wait for all ops to finish
        return Q.all(operations);
      })
      .then(function() {
        // Return dummy value
        return true;
      });
  }


  fetchRelated(thisId, direction, label, otherController) {
    thisId = parseInt(thisId);
    var others = [];

    return Q.ninvoke(db, 'relationships', thisId, direction, label)
      .then(function(relations) {
        var ops = [];

        // If the relation is incoming to this model,
        // Then the id property will be start.
        // And vice versa
        var otherIdProp = (direction === 'in') ? 'start' : 'end';

        _.each(relations, function(rel) {
          ops.push(
            otherController.get(rel[otherIdProp])
              .then(function(node) {
                others.push(node);
              })
          );
        });

        // Wait for all ops to complete
        return Q.all(ops);
      }).then(function() {
        return others;
      });
  }
}

module.exports = Controller;
