'use strict';

var Q = require('q');

module.exports = function(handler) {
  return function(req, res, next) {
    Q.when(handler(req, res))
      .then(function(response) {
        // Handler was successful
        res.json(response);
      }, function(error) {
        // Handler failed
        // res.status(400).json(error);
        next(error);
      });
  };
};
