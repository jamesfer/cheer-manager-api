'use strict';

var seraph = require('seraph');

module.exports = seraph({
  server: process.env.DB_HOST,
  user: process.env.DB_USER,
  pass: process.env.DB_PASS
});
